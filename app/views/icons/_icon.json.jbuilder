json.extract! icon, :id, :title, :image, :iconset_id, :created_at, :updated_at
json.url icon_url(icon, format: :json)
