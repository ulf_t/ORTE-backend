json.extract! map, :id, :title, :text, :published, :group_id, :created_at, :updated_at
json.url map_url(map, format: :json)
